/* eslint-disable no-undef */
// eslint-disable-next-line no-unused-vars
const { transform } = require('@babel/core')

module.exports = {
  transform: {
    '\\.js$': 'babel-jest',
  },
  testEnvironment: 'jsdom',
  moduleNameMapper: {
    '\\.(jpg|ico|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/mocks/fileMock.js',
    '\\.(css|scss)$': '<rootDir>/mocks/fileMock.js',
  },
}
