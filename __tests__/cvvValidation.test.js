import { expect, test } from '@jest/globals'
import { isValidCVV } from '../src/csvValidation'

test('Пропускает CVV с 3 символами', () => {
  expect(isValidCVV('232')).toBe(true)
})

test('Не пропускает CVV с 1-2 символами', () => {
  expect(isValidCVV('22')).toBe(false)
  expect(isValidCVV('2')).toBe(false)
})

test('Не пропускает CVV с 4+ символами', () => {
  expect(isValidCVV('2243')).toBe(false)
  expect(isValidCVV('282184')).toBe(false)
})

test('Не пропускает CVV с нецифровыми символами', () => {
  expect(isValidCVV('2ф2')).toBe(false)
  expect(isValidCVV('2ww')).toBe(false)
  expect(isValidCVV('2,8')).toBe(false)
})
