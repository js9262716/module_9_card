import { expect, test } from '@jest/globals'
import { createValidationForm } from '../src/index'

test('Проверка формы', () => {
  const form = createValidationForm()
  expect(form.childNodes.length).toBe(4)
  const labels = form.querySelectorAll('label')
  expect(labels[0].textContent).toBe('Номер карты')
  expect(labels[1].textContent).toBe('ММ/ГГ')
  expect(labels[2].textContent).toBe('CVV/CVC')
  expect(labels[3].textContent).toBe('Email')
})
