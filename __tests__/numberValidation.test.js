import { expect, test } from '@jest/globals'
import valid from 'card-validator'

test('Пропускает корректный номер карты', () => {
  expect(valid.number('2200700761452324').isValid).toBe(true)
  expect(valid.number('4276161718418402').isValid).toBe(true)
  expect(valid.number('5536914151738055').isValid).toBe(true)
})

test('Не пропускает номер, содержащий нецифровые символы', () => {
  expect(valid.number('22aщц0076,452324').isValid).toBe(false)
  expect(valid.number('427616 718;1d402').isValid).toBe(false)
})

test('Не пропускает номер с недостаточным количеством символов', () => {
  expect(valid.number('22007052').isValid).toBe(false)
})

test('Не пропускает номер с избыточным количеством символов', () => {
  expect(valid.number('2200700761452123').isValid).toBe(false)
})
