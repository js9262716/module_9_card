export function isValidCVV(cvv) {
  const regex = /^[0-9]{3}$/
  return regex.test(cvv)
}
